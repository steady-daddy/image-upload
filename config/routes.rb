Rails.application.routes.draw do
  post 'media/upload'
  get 'media/list'
  resources :users, only: %w(create)
end
