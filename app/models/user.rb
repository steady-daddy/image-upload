class User < ApplicationRecord
  PASSWORD_REGEX = /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}/
  MAX_MEDIA_UPLOAD = 5
  validates_presence_of :username
  validates_uniqueness_of :username
  validates :password, :format => {:with => PASSWORD_REGEX,
                                   :message => "must be 8 digits containing at least one uppercase letter, one lowercase letter, one digit and one special character" }, presence: true, on: :create

  before_create :generate_token
  has_many :medias

  def get_media_size
    self.medias.size
  end

  def is_allowed_to_upload?
    self.get_media_size <= User::MAX_MEDIA_UPLOAD
  end

  protected

  def generate_token
    self.auth_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless User.exists?(auth_token: random_token)
    end
  end

end