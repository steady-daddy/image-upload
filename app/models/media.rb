class Media < ApplicationRecord
  belongs_to :user
  self.table_name = "media"
  mount_uploader :image, MediaUploader
  validates_presence_of :title, :description, :image
end
