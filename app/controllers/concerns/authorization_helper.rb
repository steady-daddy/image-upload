module AuthorizationHelper
  def authenticate!
    return true if @current_user  # skip re-querying the Database
    authenticate_with_http_token do |token, options|
      @current_user = User.find_by(:auth_token => token)
    end
  end
end