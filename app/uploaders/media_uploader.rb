class MediaUploader < CarrierWave::Uploader::Base
   include CarrierWave::RMagick
   include Cloudinary::CarrierWave

   #storage :fog

   version :thumbnail do
     eager
     process resize_to_fit: [50, 50]
   end

   def public_id
     model.class.to_s
   end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
   def extension_whitelist
     %w(jpg jpeg gif png)
   end
end
